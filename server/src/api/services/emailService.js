import { emailTransporter } from '../../helpers/emailTransporter';
import { createToken } from '../../helpers/tokenHelper';
import { ErrorHandlerBadRequest } from '../../error/ErrorHandlerBadRequest';

const baseClientURL = 'http://localhost:3000';

const passwordChangeLink = userId => {
  const token = createToken({ id: userId });
  return `${baseClientURL}/password/change/${token}`;
};

const sharePostLink = postId => `${baseClientURL}/share/${postId}`;

const passwordChangeLinkTemplate = link => `<p style="
                      background-color: #008CBA;
                      border: none;
                      padding: 15px 32px;
                      text-align: center;
                      text-decoration: none;
                      display: inline-block;
                      font-size: 16px;
                      border-radius: 4px;">
                      <a style="color: white" href=${link}>To change your password, follow this link</a>
                      </p>`;

const sharePostLinkTemplate = link => `<p>
                      <a href=${link}>Your post...</a>
                      </p>`;

export const sendChangePasswordLink = async user => {
  try {
    const { email, id } = user;
    const link = passwordChangeLink(id);
    const html = passwordChangeLinkTemplate(link);
    emailTransporter(email, 'Change password link', html);
    return Promise.resolve({ message: 'Click on the link sent to your email' });
  } catch (e) {
    return Promise.reject(new ErrorHandlerBadRequest(e.message));
  }
};

export const sendLikedPostLink = async (postId, email, isLike) => {
  try {
    const link = sharePostLink(postId);
    const html = sharePostLinkTemplate(link);
    emailTransporter(email, html, isLike);
  } catch (e) {
    throw Error(e.message);
  }
};
