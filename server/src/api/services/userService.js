import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, statusText } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, statusText };
};

export const getOne = async data => {
  const user = await userRepository.getOne(data);
  return user;
};

export const update = async (userId, data) => {
  const user = await userRepository.updateById(userId, data);
  return user;
};
