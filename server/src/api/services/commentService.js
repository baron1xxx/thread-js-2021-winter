import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const update = (id, post) => commentRepository.updateById(id, post);
export const remove = id => commentRepository.updateById(id, { isDeleted: true });

export const setReaction = async (userId, { commentId, isLike = true }) => {
  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  return Number.isInteger(result) ? { commentId } : commentReactionRepository.getCommentReaction(userId, commentId);
};

export const getReaction = async (userId, commentId) => {
  const commentReaction = await commentReactionRepository.getCommentReaction(userId, commentId);
  return commentReaction || {};
};
