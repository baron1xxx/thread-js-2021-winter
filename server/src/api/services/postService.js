import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import * as emailService from './emailService';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const update = (id, post) => postRepository.updateById(id, post);
export const remove = id => postRepository.updateById(id, { isDeleted: true });

export const setReaction = async (userId, { postId, isLike = true }) => {
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  if (!Number.isInteger(result)) {
    // const { user: { email } } = await postReactionRepository.getPostReaction(userId, postId);
    // eslint-disable-next-line no-unused-vars
    const { user: { email } } = await postRepository.getPostById(postId);
    await emailService.sendLikedPostLink(postId, email, isLike);
  }

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? { postId } : postReactionRepository.getPostReaction(userId, postId);
};

export const getReaction = async (userId, postId) => {
  const postReaction = await postReactionRepository.getPostReaction(userId, postId);
  return postReaction || {};
};

