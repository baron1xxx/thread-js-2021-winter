import { Router } from 'express';
import * as postService from '../services/postService';
import createPostValidMiddleware from '../middlewares/postMiddleware/createPostValidMiddleware';
import updatePostValidMiddleware from '../middlewares/postMiddleware/updatePostValidMiddleware';
import checkAccessMiddleware from '../middlewares/checkAccessMiddleware';

const router = Router();

export default router;

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    // .then(posts => res.send({ posts, length: posts.length }))
    .then(posts => res.send(posts))
    .catch(next))
  .get('/react/:id', (req, res, next) => postService.getReaction(req.user.id, req.params.id)
    .then(reaction => res.send(reaction))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', createPostValidMiddleware, (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        if (reaction.isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
        }
      }
      req.io.emit('post_reacted', reaction.postId);
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id',
    updatePostValidMiddleware,
    checkAccessMiddleware, (req, res, next) => postService.update(req.params.id, req.body)
      .then(post => {
        req.io.emit('update_post', post.id);
        return res.send(post);
      })
      .catch(next))
  .delete('/:id', checkAccessMiddleware, (req, res, next) => postService.remove(req.params.id)
    .then(post => {
      req.io.emit('remove_post', post.id);
      return res.send(post);
    })
    .catch(next));
