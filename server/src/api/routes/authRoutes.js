import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import * as emailService from '../services/emailService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';
import checkEmailMiddleware from '../middlewares/checkEmailMiddleware';
import emailValidMiddleware from '../middlewares/userMiddleware/emailValidMiddleware';
import changePasswordValidMiddleware from '../middlewares/changePasswordValidMiddleware';
import checkTokenMiddleware from '../middlewares/checkTokenlMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/password/forgot',
    emailValidMiddleware,
    checkEmailMiddleware, (req, res, next) => emailService.sendChangePasswordLink(req.user)
      .then(data => res.send(data))
      .catch(next))
  .post('/password/change/:token',
    changePasswordValidMiddleware,
    checkTokenMiddleware,
    (req, res, next) => authService.changePassword(req.user.id, req.body)
      .then(data => res.send(data))
      .catch(next))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next));

export default router;
