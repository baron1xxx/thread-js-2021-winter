import { Router } from 'express';
import * as commentService from '../services/commentService';
import createCommentValidMiddleware from '../middlewares/commentMiddleware/createCommentValidMiddleware';
import updateCommentValidMiddleware from '../middlewares/commentMiddleware/updateCommentValidMiddleware';
import checkAccessMiddleware from '../middlewares/checkAccessMiddleware';

const router = Router();

router
  .get('/react/:id', (req, res, next) => commentService.getReaction(req.user.id, req.params.id)
    .then(reaction => res.send(reaction))
    .catch(next))
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', createCommentValidMiddleware, (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => {
      req.io.emit('new_comment', comment.id);
      return res.send(comment);
    })
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      req.io.emit('comment_react', reaction.commentId);
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', checkAccessMiddleware, updateCommentValidMiddleware,
    (req, res, next) => commentService.update(req.params.id, req.body)
      .then(comment => {
        req.io.emit('update_comment', comment.id);
        return res.send(comment);
      })
      .catch(next))
  .delete('/:id', checkAccessMiddleware, (req, res, next) => commentService.remove(req.params.id)
    .then(comment => {
      req.io.emit('remove_comment', comment.id);
      return res.send(comment);
    })
    .catch(next));

export default router;
