import { Router } from 'express';
import * as userService from '../services/userService';
import updateUserValidMiddleware from '../middlewares/userMiddleware/updateUserValidMiddleware';
import checkUpdateUserMiddleware from '../middlewares/userMiddleware/checkUpdateUserMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .get('/:id', (req, res, next) => userService.getUserById(req.params.id)
    .then(data => res.send(data))
    .catch(next))
  .put('/:id',
    checkUpdateUserMiddleware,
    updateUserValidMiddleware, (req, res, next) => userService.update(req.user.id, req.body)
      .then(data => res.send(data))
      .catch(next));

export default router;
