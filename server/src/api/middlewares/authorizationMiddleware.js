import jwtMiddleware from './jwtMiddleware';

export default (routesWhiteList = []) => (req, res, next) => (
  // ( req.path.split('/').slice(0, 4).join('/') Allow links with parameters for an unauthorized user.
  routesWhiteList.some(route => route === req.path.split('/').slice(0, 4).join('/'))
    ? next()
    : jwtMiddleware(req, res, next) // auth the user if requested path isn't from the white list
);
