import userRepository from '../../data/repositories/userRepository';
import { ErrorHandlerUnauthorized } from '../../error/ErrorHandlerUnauthorized';
import { ErrorHandler } from '../../error/ErrorHandler';
import { parseToken } from '../../helpers/tokenHelper';

export default async (req, res, next) => {
  try {
    const { token } = req.params;
    const { id } = parseToken(token);
    const user = await userRepository.getUserById(id);
    if (!user) {
      return next(new ErrorHandlerUnauthorized('User not exists.'));
    }
    req.user = user;
    return next();
  } catch (e) {
    return next(new ErrorHandler(e.status, e.message));
  }
};
