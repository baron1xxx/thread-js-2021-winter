import userRepository from '../../data/repositories/userRepository';
import { ErrorHandlerUnauthorized } from '../../error/ErrorHandlerUnauthorized';
import { ErrorHandler } from '../../error/ErrorHandler';

export default async (req, res, next) => {
  try {
    const { body: { email } } = req;
    const user = await userRepository.getByEmail(email);
    if (!user) {
      return next(new ErrorHandlerUnauthorized('Email not exists.'));
    }
    req.user = user;
    return next();
  } catch (e) {
    return next(new ErrorHandler(e.status, e.message));
  }
};
