import { changePasswordValidator } from '../../validators/changePasswordValidator';
import { ErrorHandlerUnauthorized } from '../../error/ErrorHandlerUnauthorized';
import { ErrorHandler } from '../../error/ErrorHandler';

export default (req, res, next) => {
  try {
    const { error } = changePasswordValidator.validate(req.body);
    return error
      ? next(new ErrorHandlerUnauthorized(error.details[0].message))
      : next();
  } catch (e) {
    return next(new ErrorHandler(e.status, e.message));
  }
};
