import * as commentService from '../services/commentService';
import * as postService from '../services/postService';
import { ErrorHandlerBadRequest } from '../../error/ErrorHandlerBadRequest';
import { ErrorHandler } from '../../error/ErrorHandler';

export default async (req, res, next) => {
  try {
    const { user: { id: userId } } = req;
    const { id } = req.params;
    const arrUrl = req.originalUrl.split('/');
    const urlElement = arrUrl[arrUrl.length - 2];

    if (urlElement === 'posts') {
      const post = await postService.getPostById(id);
      if (!post) {
        return next(new ErrorHandlerBadRequest('Post not exist'));
      }
      return post.userId !== userId
        ? next(new ErrorHandlerBadRequest('You do not have permission to change this resource.'))
        : next();
    }

    const comment = await commentService.getCommentById(id);
    if (!comment) {
      return next(new ErrorHandlerBadRequest('Comment not exist'));
    }
    return comment.userId !== userId
      ? next(new ErrorHandlerBadRequest('You d\'not have permission to change this resource'))
      : next();
  } catch (e) {
    return next(new ErrorHandler(e.status, e.message));
  }
};
