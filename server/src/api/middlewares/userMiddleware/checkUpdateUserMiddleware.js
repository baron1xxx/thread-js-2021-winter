import * as userService from '../../services/userService';
import { ErrorHandlerBadRequest } from '../../../error/ErrorHandlerBadRequest';
import { ErrorHandlerNotFound } from '../../../error/ErrorHandlerNotFound';
import { ErrorHandler } from '../../../error/ErrorHandler';

export default async (req, res, next) => {
  try {
    const { user: { id: userId } } = req;
    const { username, email } = req.body;

    const user = await userService.getUserById(userId);
    if (!user) {
      return next(new ErrorHandlerNotFound('User not exists.'));
    }
    if (username) {
      const userName = await userService.getOne({ username });
      if (userName && userName.id !== userId) {
        return next(new ErrorHandlerBadRequest('User with username already exists.'));
      }
    }

    if (email) {
      const userEmail = await userService.getOne({ email });
      if (userEmail && userEmail.id !== userId) {
        return next(new ErrorHandlerBadRequest('User with email already exists.'));
      }
    }
    return next();
  } catch (e) {
    return next(new ErrorHandler(e.status, e.message));
  }
};
