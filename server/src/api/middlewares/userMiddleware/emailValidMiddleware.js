import { emailValidator } from '../../../validators/emailValidator';
import { ErrorHandlerUnauthorized } from '../../../error/ErrorHandlerUnauthorized';
import { ErrorHandler } from '../../../error/ErrorHandler';

export default (req, res, next) => {
  try {
    const { error } = emailValidator.validate(req.body);
    return error
      ? next(new ErrorHandlerUnauthorized(error.details[0].message)) // Joi error parse
      : next();
  } catch (e) {
    return next(new ErrorHandler(e.status, e.message));
  }
};
