import { updateCommentValidator } from '../../../validators/commentValidator';
import { ErrorHandlerBadRequest } from '../../../error/ErrorHandlerBadRequest';
import { ErrorHandler } from '../../../error/ErrorHandler';

export default (req, res, next) => {
  try {
    const { error } = updateCommentValidator.validate(req.body);
    return error
      ? next(new ErrorHandlerBadRequest(error.details[0].message))
      : next();
  } catch (e) {
    return next(new ErrorHandler(e.status, e.message));
  }
};
