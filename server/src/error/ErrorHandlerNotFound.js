import { ErrorHandler } from './ErrorHandler';

export class ErrorHandlerNotFound extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 404;
    this.message = message;
  }
}
