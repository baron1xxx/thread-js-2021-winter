import { ErrorHandler } from './ErrorHandler';

export class ErrorHandlerForbidden extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 403;
    this.message = message;
  }
}
