import { ErrorHandler } from './ErrorHandler';

export class ErrorHandlerBadRequest extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 400;
    this.message = message;
  }
}
