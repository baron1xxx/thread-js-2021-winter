import { ErrorHandler } from './ErrorHandler';

export class ErrorHandlerUnauthorized extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 401;
    this.message = message;
  }
}
