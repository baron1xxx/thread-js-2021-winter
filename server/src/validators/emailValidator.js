import Joi from 'joi';

export const emailValidator = Joi.object({
  email: Joi
    .string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .messages({
      'string.base': 'Email must be string',
      'string.email': 'Email must be  valid'
    })
});
