import Joi from 'joi';

export const postCreateValidator = Joi.object({
  body: Joi
    .string()
    .required()
    .min(1)
    .max(200)
    .regex(new RegExp('^[a-zA-Z0-9?$@#()\'!,+\\-=_:.&€£*%\\s]+$'))
    .messages({
      'string.base': 'body must be string',
      'string.min': 'Minimum {#limit} symbol ',
      'string.max': 'Maximum {#limit} symbol ',
      'string.pattern.base': 'Does not match pattern.',
      'string.required': 'body is required'
    }),
  imageId: Joi
    .string()
    .guid()
});

export const postUpdateValidator = Joi.object({
  body: Joi
    .string()
    .required()
    .min(1)
    .max(200)
    .regex(new RegExp('^[a-zA-Z0-9?$@#()\'!,+\\-=_:.&€£*%\\s]+$'))
    .messages({
      'string.base': 'body must be string',
      'string.min': 'Minimum {#limit} symbol ',
      'string.max': 'Maximum {#limit} symbol ',
      'string.pattern.base': 'Does not match pattern.',
      'string.required': 'body is required'
    }),
  userId: Joi
    .string()
    .required()
    .guid()
    .messages({
      'string.base': 'userId must be string',
      'string.required': 'userId is required'
    }),
  imageId: Joi
    .string()
    .guid()
});
