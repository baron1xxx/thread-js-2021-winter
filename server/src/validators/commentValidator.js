import Joi from 'joi';

export const createCommentValidator = Joi.object({
  postId: Joi
    .string()
    .required()
    .guid()
    .messages({
      'string.base': 'postId must be string',
      'string.required': 'postId is required'
    }),
  body: Joi
    .string()
    .required()
    .min(1)
    .max(200)
    .regex(new RegExp('^[a-zA-Z0-9?$@#()\'!,+\\-=_:.&€£*%\\s]+$'))
    .messages({
      'string.base': 'body must be string',
      'string.min': 'Minimum {#limit} symbol ',
      'string.max': 'Maximum {#limit} symbol ',
      'string.pattern.base': 'Does not match pattern.',
      'string.required': 'body is required'
    })
});

export const updateCommentValidator = Joi.object({
  body: Joi
    .string()
    .required()
    .min(1)
    .max(200)
    .regex(new RegExp('^[a-zA-Z0-9?$@#()\'!,+\\-=_:.&€£*%\\s]+$'))
    .messages({
      'string.base': 'body must be string',
      'string.min': 'Minimum {#limit} symbol ',
      'string.max': 'Maximum {#limit} symbol ',
      'string.pattern.base': 'Does not match pattern.',
      'string.required': 'body is required'
    })
});
