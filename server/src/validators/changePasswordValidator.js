import Joi from 'joi';

export const changePasswordValidator = Joi.object({
  password: Joi
    .string()
    .required()
    .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,})/)
    .messages({
      'string.base': 'Password must be string',
      'string.required': 'Password is required',
      'string.pattern.base': 'Does not match pattern.'
    }),
  passwordConfirm: Joi
    .string()
    .equal(Joi.ref('password'))
    .required()
    .messages({
      'string.base': 'Confirmation password must be string',
      'string.required': 'Confirmation password is required',
      'any.only': 'Confirmation password not equals password.'
    })
});
