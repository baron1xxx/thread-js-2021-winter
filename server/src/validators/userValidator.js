import Joi from 'joi';

export const userCreateValidator = Joi.object({
  body: Joi
    .string()
    .required()
    .min(1)
    .max(200)
    .regex(new RegExp('^[a-zA-Z0-9?$@#()\'!,+\\-=_:.&€£*%\\s]+$'))
    .messages({
      'string.base': 'body must be string',
      'string.min': 'Minimum {#limit} symbol ',
      'string.max': 'Maximum {#limit} symbol ',
      'string.pattern.base': 'Does not match pattern.',
      'string.required': 'body is required'
    }),
  imageId: Joi
    .string()
    .guid()
});

export const userUpdateValidator = Joi.object({
  username: Joi
    .string()
    .min(3)
    .max(20)
    .regex(new RegExp('^[a-zA-Z0-9?$@#()\'!,+\\-=_:.&€£*%\\s]+$'))
    .messages({
      'string.base': 'Username must be string',
      'string.min': 'Username minimum {#limit} symbol ',
      'string.max': 'Username maximum {#limit} symbol ',
      'string.pattern.base': 'Does not match pattern.'
    }),
  email: Joi
    .string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .messages({
      'string.base': 'Email must be string',
      'string.email': 'Email must bea valid'
    }),
  statusText: Joi
    .string()
    .min(3)
    .max(100)
    .regex(new RegExp('^[a-zA-Z0-9?$@#()\'!,+\\-=_:.&€£*%\\s]+$'))
    .messages({
      'string.base': 'Status text must be string',
      'string.min': 'Minimum {#limit} symbol ',
      'string.max': 'Maximum {#limit} symbol ',
      'string.pattern.base': 'Does not match pattern.'
    }),
  imageId: Joi
    .string()
    .guid()
});
