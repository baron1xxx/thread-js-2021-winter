export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('posts', 'isDeleted',
        {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: false
        }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('posts', 'isDeleted', { transaction })
    ]))
};
