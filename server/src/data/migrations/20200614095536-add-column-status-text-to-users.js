export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'statusText',
        {
          allowNull: true,
          type: Sequelize.TEXT,
          defaultValue: ''
        }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'statusText', { transaction })
    ]))
};
