import { PostReactionModel, PostModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id',
        'user.id'
      ],
      where: { userId, postId },
      include: [
        {
          model: PostModel,
          attributes: ['id', 'userId']
        },
        {
          model: UserModel,
          attributes: ['id', 'email']
        }
      ]
    });
  }

  // Test
  getUsersPostReact(postId, isLike) {
    return this.model.findAll({
      attributes: ['isLike'],
      where: {
        postId,
        isLike
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username', 'imageId'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }
      ]
    });
  }

  getAllPostReact() {
    return this.model.findAll({
      group: [
        'postReaction.id',
        'postId'
      ]
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
