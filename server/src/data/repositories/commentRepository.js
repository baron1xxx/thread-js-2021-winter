import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';
import sequelize from '../db/connection';

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id',
        'commentReactions.id',
        'commentReactions->user.id',
        'commentReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                         FROM "commentReactions" as "react"
                         WHERE "react"."commentId" = "comment"."id" AND "react"."isLike" = true )`), 'likeCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                         FROM "commentReactions" as "react"
                         WHERE "react"."commentId" = "comment"."id" AND "react"."isLike" = false )`), 'dislikeCount']
        ]
      },
      include: [
        {
          model: UserModel,
          attributes: ['id', 'username', 'statusText'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: CommentReactionModel,
          attributes: ['id', 'isLike'],
          include: {
            model: UserModel,
            attributes: ['id', 'username', 'statusText'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        }
      ]
    });
  }
}

export default new CommentRepository(CommentModel);
