import Sequelize from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const { Op } = Sequelize;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      notEqualUserId,
      likedPostsAuthUser
    } = filter;

    const where = { isDeleted: false };
    if (userId) {
      Object.assign(where, { userId });
    }

    if (notEqualUserId) {
      Object.assign(where, {
        userId: {
          [Op.ne]: notEqualUserId
        } });
    }

    let postReactionFilter;
    const reactFilter = {};
    if (likedPostsAuthUser) {
      Object.assign(reactFilter, { userId: likedPostsAuthUser, isLike: true });
      postReactionFilter = reactFilter;
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "postReactions" as "react"
                        WHERE "react"."postId" = "post"."id" AND "react"."isLike" = true )`), 'likeCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "postReactions" as "react"
                        WHERE "react"."postId" = "post"."id" AND "react"."isLike" = false )`), 'dislikeCount']
        ]
      },
      include: [
        {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include:
          {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: PostReactionModel,
          attributes: ['id', 'isLike', 'userId'],
          where: postReactionFilter,
          include: [
            {
              model: UserModel,
              attributes: ['id', 'username'],
              include: {
                model: ImageModel,
                attributes: ['id', 'link']
              }
            }
          ]
        }
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'comments->commentReactions.id',
        'comments->commentReactions->user.id',
        'comments->commentReactions->user->image.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"  AND "comment"."isDeleted" = false)`), 'commentCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                         FROM "postReactions" as "react"
                         WHERE "react"."postId" = "post"."id" AND "react"."isLike" = true )`), 'likeCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                         FROM "postReactions" as "react"
                         WHERE "react"."postId" = "post"."id" AND "react"."isLike" = false )`), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        required: false,
        where: {
          isDeleted: false
        },
        attributes: {
          include: [
            [sequelize.literal(`
                        (SELECT COUNT(*)
                         FROM "commentReactions" as "react"
                         WHERE "react"."commentId" = "comments"."id" AND "react"."isLike" = true )`), 'likeCount'],
            [sequelize.literal(`
                        (SELECT COUNT(*)
                         FROM "commentReactions" as "react"
                         WHERE "react"."commentId" = "comments"."id" AND "react"."isLike" = false )`), 'dislikeCount']
          ]
        },
        include: [
          {
            model: UserModel,
            attributes: ['id', 'username', 'statusText'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          },
          {
            model: CommentReactionModel,
            attributes: ['id', 'isLike'],
            include: {
              model: UserModel,
              attributes: ['id', 'username', 'statusText'],
              include: {
                model: ImageModel,
                attributes: ['id', 'link']
              }
            }
          }
        ]
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'email'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        // attributes: []
        attributes: ['id', 'isLike'],
        include: {
          model: UserModel,
          attributes: ['id', 'username', 'statusText'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }]
    });
  }
}

export default new PostRepository(PostModel);
