import { CommentReactionModel, CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getUsersCommentReact(commentId, isLike) {
    return this.model.findAll({
      attributes: ['isLike'],
      where: {
        commentId,
        isLike
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username', 'imageId'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }
      ]
    });
  }

  getAllCommentReact() {
    return this.model.findAll({
      group: [
        'postReaction.id',
        'postId'
      ]
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
