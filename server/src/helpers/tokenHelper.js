import jwt from 'jsonwebtoken';
import { secret, expiresIn } from '../config/jwtConfig';

export const createToken = data => jwt.sign(data, secret, { expiresIn });

export const parseToken = token => jwt.verify(token, secret, (err, decode) => {
  if (err) throw new Error('Token is invalid.');
  return decode;
});
