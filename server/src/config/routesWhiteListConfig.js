export default [
  '/auth/login',
  '/auth/register',
  '/auth/password/forgot',
  '/auth/password/change'
];
