import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';
import { ChangePasswordSchema } from '../../helpers/validatorHelper';
import ErrorMessage from '../ErrorMessage';
import SuccessMessage from '../SuccessMessage';

const ChangePasswordForm = ({ token, changePassword }) => {
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState(undefined);
  const [error, setError] = useState(false);

  const passwordChanged = data => {
    setPassword(data);
  };

  const confirmPasswordChanged = data => {
    setPasswordConfirm(data);
  };

  const handleChangePasswordClick = async () => {
    setIsLoading(true);

    try {
      ChangePasswordSchema.validateSync({ password, passwordConfirm }); // If error throw Error()
      const { message } = await changePassword({ password, passwordConfirm }, token);
      setSuccessMessage({ message });
      setPassword('');
      setPasswordConfirm('');
      setError(false);
    } catch ({ message }) {
      setError({ message });
      setIsLoading(false);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div>
      {error && <ErrorMessage error={error.message} close={setError} />}
      {successMessage && <SuccessMessage successMessage={successMessage.message} close={setSuccessMessage} />}
      <Form name="loginForm" size="large" onSubmit={handleChangePasswordClick}>
        <Segment>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            value={password}
            onChange={ev => passwordChanged(ev.target.value)}
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Confirm password"
            type="password"
            value={passwordConfirm}
            onChange={ev => confirmPasswordChanged(ev.target.value)}
          />
          <Button
            type="submit"
            color="teal"
            fluid
            size="large"
            loading={isLoading}
            primary
          >
            Change password
          </Button>
        </Segment>
      </Form>
    </div>

  );
};

ChangePasswordForm.propTypes = {
  changePassword: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired
};

export default ChangePasswordForm;
