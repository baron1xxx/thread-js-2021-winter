import React from 'react';
import PropTypes from 'prop-types';
import { Message, Icon } from 'semantic-ui-react';
import styles from '../DeletePost/styles.module.scss';

const ErrorMessage = ({ error, close }) => (error && (
  <Message className={styles.info} negative>
    <Message.Header>{error}</Message.Header>
    <Icon name="close" onClick={() => close(false)} />
  </Message>
)
);

ErrorMessage.propTypes = {
  error: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default ErrorMessage;

