import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, Grid, Segment, Message } from 'semantic-ui-react';

import styles from './styles.module.scss';

const DeletePost = ({ deletePost, removePost, close, postId }) => {
  const [error, setError] = useState(false);

  const handleDeletePost = async id => {
    try {
      await removePost(id);
      deletePost();
    } catch (e) {
      setError(e.message);
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={close}>
      <Modal.Header>
        {!error && (
          <Message className={styles.info} info>
            <Message.Header>Are you serious?</Message.Header>
          </Message>
        )}
        {error && (
          <Message className={styles.info} negative>
            <Message.Header>{error}</Message.Header>
          </Message>
        )}
      </Modal.Header>
      <Modal.Content>
        <Grid columns="equal">
          <Grid.Row>
            <Grid.Column>
              <Segment>
                <Button className={styles.button} negative onClick={() => deletePost()}>Cancel</Button>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
                <Button className={styles.button} positive onClick={() => handleDeletePost(postId)}>Delete</Button>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

DeletePost.propTypes = {
  deletePost: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export default DeletePost;

