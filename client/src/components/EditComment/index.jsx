import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Modal, Segment } from 'semantic-ui-react';
import { CommentSchema } from '../../helpers/validatorHelper';
import ErrorMessage from '../ErrorMessage';

import styles from './styles.module.scss';

const EditComment = ({ comment, updateComment, close }) => {
  const { id } = comment;
  const [body, setBody] = useState(comment.body);
  const [isBodyValid, setIsBodyValid] = useState(true);
  const [error, setError] = useState(false);

  const handleEditComment = async () => {
    try {
      await updateComment(id, { body });
      close();
    } catch (e) {
      setError({ message: e.message });
    }
  };
  const handleChangeComment = async bodyComment => {
    setBody(bodyComment);
    CommentSchema.validate({ body: bodyComment })
      .then(() => {
        setIsBodyValid(true);
        setError(false);
      })
      .catch(({ message }) => {
        setError({ message });
        setIsBodyValid(false);
      });
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={close}>
      <Modal.Content>
        {error && <ErrorMessage error={error.message} close={setError} />}
        <Segment>
          <Form onSubmit={handleEditComment}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="Edit is the comment?"
              onChange={ev => handleChangeComment(ev.target.value)}
            />
            <Button
              className={styles.btn}
              floated="left"
              color="red"
              onClick={close}
            >
              Cancel
            </Button>
            <Button
              className={styles.btn}
              disabled={!body || !isBodyValid}
              floated="right"
              color="blue"
              type="submit"
            >
              Edit
            </Button>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

EditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditComment;
