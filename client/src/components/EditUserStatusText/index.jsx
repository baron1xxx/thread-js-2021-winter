import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Modal, Segment } from 'semantic-ui-react';
import { UpdateUserSchema } from '../../helpers/validatorHelper';
import ErrorMessage from '../ErrorMessage';

// TODO edit code

const EditUserStatusText = ({ statusText, close }) => {
  const [userStatusText, setUserStatusText] = useState(statusText);
  // eslint-disable-next-line no-unused-vars
  const [isUploading, setIsUploading] = useState(false);
  const [isBodyValid, setIsBodyValid] = useState(true);
  const [error, setError] = useState(false);

  const userStatusTextChanged = data => {
    setUserStatusText(data);
    UpdateUserSchema.validate({ statusText: data })
      .then(() => {
        setIsBodyValid(true);
        setError(false);
      })
      .catch(({ message }) => {
        setError({ message });
        setIsBodyValid(false);
      });
  };

  const handleEditStatusText = async () => {
    // TODO
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => close()}>
      <Modal.Content>
        {error && <ErrorMessage error={error.message} close={setError} />}
        <Segment>
          <Form onSubmit={handleEditStatusText}>
            <Form.Input
              name="body"
              value={userStatusText}
              placeholder="Edit is the news?"
              onChange={ev => userStatusTextChanged(ev.target.value)}
            />
            <Button
              disabled={!statusText || !isBodyValid}
              color="blue"
              type="submit"
            >
              Edit status text
            </Button>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

EditUserStatusText.propTypes = {
  statusText: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default EditUserStatusText;
