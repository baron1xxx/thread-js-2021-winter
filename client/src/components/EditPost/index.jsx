import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Icon, Image, Modal, Segment } from 'semantic-ui-react';
import styles from './styles.module.scss';
import { PostSchema } from '../../helpers/validatorHelper';
import ErrorMessage from '../ErrorMessage';

const EditPost = ({ post, close, updatePost, uploadImage }) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState({ imageLink: post.image?.link });
  const [isUploading, setIsUploading] = useState(false);
  const [isBodyValid, setIsBodyValid] = useState(true);
  const [error, setError] = useState(false);

  const postBodyChanged = data => {
    setBody(data);
    PostSchema.validate({ body: data })
      .then(() => {
        setIsBodyValid(true);
        setError(false);
      })
      .catch(({ message }) => {
        setError({ message });
        setIsBodyValid(false);
      });
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } catch (e) {
      setError({ message: e.message });
      setIsUploading(false);
    } finally {
      setIsUploading(false);
    }
  };

  const handleEditPost = async () => {
    try {
      if (!body) {
        return;
      }
      await updatePost(post.id, { imageId: image?.imageId, body, userId: post.userId });
      close();
    } catch (e) {
      setError({
        message: e.message
      });
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => close()}>
      <Modal.Content>
        {error && <ErrorMessage error={error.message} close={setError} />}
        <Segment>
          <Form onSubmit={handleEditPost}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="Edit is the news?"
              onChange={ev => postBodyChanged(ev.target.value)}
            />
            {image?.imageLink && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.imageLink} alt="post" />
              </div>
            )}
            <Button color="teal" icon labelPosition="left" loading={isUploading} disabled={isUploading} as="label">
              <Icon name="image" />
              Attach image
              <input name="image" type="file" onChange={handleUploadFile} hidden />
            </Button>
            <Button disabled={!body || !isBodyValid} floated="right" color="blue" type="submit">Edit Post</Button>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default EditPost;
