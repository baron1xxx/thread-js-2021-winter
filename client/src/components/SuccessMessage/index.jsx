import React from 'react';
import PropTypes from 'prop-types';
import { Message, Icon } from 'semantic-ui-react';
import styles from '../DeletePost/styles.module.scss';

const SuccessMessage = ({ successMessage, close }) => (successMessage && (
  <Message className={styles.info} success>
    <Message.Header>{successMessage}</Message.Header>
    <Icon name="close" onClick={() => close(undefined)} />
  </Message>
)
);

SuccessMessage.propTypes = {
  successMessage: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SuccessMessage;

