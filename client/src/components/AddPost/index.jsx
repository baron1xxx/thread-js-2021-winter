import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import { PostSchema } from '../../helpers/validatorHelper';
import ErrorMessage from '../ErrorMessage';

import styles from './styles.module.scss';

const AddPost = ({
  addPost,
  uploadImage
}) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);
  const [isBodyValid, setIsBodyValid] = useState(true);
  const [error, setError] = useState(false);

  const postBodyChanged = data => {
    setBody(data);
    PostSchema.validate({ body: data })
      .then(() => {
        setIsBodyValid(true);
        setError(false);
      })
      .catch(({ message }) => {
        setError({ message });
        setIsBodyValid(false);
      });
  };

  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    try {
      await addPost({ imageId: image?.imageId, body });
      setBody('');
      setImage(undefined);
    } catch (e) {
      setError({
        message: e.message
      });
    }
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } catch (e) {
      setError({ message: e.message });
      setIsUploading(false);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleAddPost}>
        {error && <ErrorMessage error={error.message} close={setError} />}
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => postBodyChanged(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit" disabled={!body || !isBodyValid}>Post</Button>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;
