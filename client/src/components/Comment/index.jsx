import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import UsersListPopup from '../UsersListPopup';

import styles from './styles.module.scss';

const Comment = ({ comment, userId, showEditComment, commentId, likeComment, dislikeComment }) => {
  const { id, body, createdAt, user, likeCount, dislikeCount, commentReactions } = comment;

  const likeCommentReactions = commentReactions.filter(reaction => reaction.isLike) || [];
  const dislikeCommentReactions = commentReactions.filter(reaction => !reaction.isLike) || [];

  const [isLikePopupOpen, setIsLikePopupOpen] = useState(false);
  const [isDislikePopupOpen, setIsDislikePopupOpen] = useState(false);

  const handleLikeMouseOver = () => {
    setIsLikePopupOpen(true);
  };
  const handleLikeMouseOut = () => {
    setIsLikePopupOpen(false);
  };

  const handleDislikeMouseOver = () => {
    setIsDislikePopupOpen(true);
  };
  const handleDislikeMouseOut = () => {
    setIsDislikePopupOpen(false);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
          <br />
          {user.statusText}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onMouseOver={handleLikeMouseOver}
          onMouseOut={handleLikeMouseOut}
          onFocus={() => undefined}
          onBlur={() => undefined}
          onClick={() => likeComment(id)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onMouseOver={handleDislikeMouseOver}
          onMouseOut={handleDislikeMouseOut}
          onFocus={() => undefined}
          onBlur={() => undefined}
          onClick={() => dislikeComment(id)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        { user.id === userId && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => showEditComment(comment)}>
            <Icon name="edit" />
          </Label>
        ) }
        { user.id === userId && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => commentId(comment.id)}>
            <Icon name="trash alternate" />
          </Label>
        ) }
        {(isLikePopupOpen && !!likeCommentReactions.length)
        && (
          <UsersListPopup reactions={likeCommentReactions} />
        )}
        {(isDislikePopupOpen && !!dislikeCommentReactions.length)
        && (
          <UsersListPopup reactions={dislikeCommentReactions} />
        )}
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  showEditComment: PropTypes.func.isRequired,
  commentId: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
