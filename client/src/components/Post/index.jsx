import React, { useState, memo } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';
import UsersListPopup from '../UsersListPopup';

const Post = memo(({ userId, post, likePost, dislikePost, toggleExpandedPost, sharePost, editedPost, deletePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    postReactions = [],
    createdAt
  } = post;

  const date = moment(createdAt).fromNow();

  const likePostReactions = postReactions.filter(reaction => reaction.isLike) || [];
  const dislikePostReactions = postReactions.filter(reaction => !reaction.isLike) || [];

  const [isLikePopupOpen, setIsLikePopupOpen] = useState(false);
  const [isDislikePopupOpen, setIsDislikePopupOpen] = useState(false);

  const handleLikeMouseOver = () => {
    setIsLikePopupOpen(true);
  };
  const handleLikeMouseOut = () => {
    setIsLikePopupOpen(false);
  };

  const handleDislikeMouseOver = () => {
    setIsDislikePopupOpen(true);
  };
  const handleDislikeMouseOut = () => {
    setIsDislikePopupOpen(false);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <div className={styles.content}>
        <Card.Content extra>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onMouseOver={handleLikeMouseOver}
            onMouseOut={handleLikeMouseOut}
            onFocus={() => undefined}
            onBlur={() => undefined}
            onClick={() => likePost(id)}
          >
            <Icon name="thumbs up" />
            {likeCount}
          </Label>
          <div className={styles.likeContent}>
            {(isLikePopupOpen && !!likePostReactions.length)
            && (
              <UsersListPopup reactions={likePostReactions} />
            )}
            {(isDislikePopupOpen && !!dislikePostReactions.length)
            && (
              <UsersListPopup reactions={dislikePostReactions} />
            )}
          </div>
          <Label
            basic
            size="small"
            as="a"
            onMouseOver={handleDislikeMouseOver}
            onMouseOut={handleDislikeMouseOut}
            onFocus={() => undefined}
            onBlur={() => undefined}
            className={styles.toolbarBtn}
            onClick={() => dislikePost(id)}
          >
            <Icon name="thumbs down" />
            {dislikeCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
            <Icon name="comment" />
            {commentCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
            <Icon name="share alternate" />
          </Label>
          { user.id === userId && (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => editedPost(post)}>
              <Icon name="edit" />
            </Label>
          ) }
          { user.id === userId && (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
              <Icon name="trash alternate" />
            </Label>
          ) }
        </Card.Content>
      </div>

    </Card>
  );
});

Post.propTypes = {
  userId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
