import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';
import EditUserStatusText from '../EditUserStatusText';

const Header = ({ user, logout }) => {
  const [userStatusText, setUserStatusText] = useState(undefined);

  const showEditStatusText = () => {
    setUserStatusText(user.statusText);
  };
  // TODO Function that will change the statusText (connect({editStatusText})(Header))
  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <HeaderUI className={styles.avatarInfo}>
                <Image circular src={getUserImgLink(user.image)} />
                <div>
                  {user.username}
                  <br />
                  {user.statusText && (
                    <div className={styles.statusText}>
                      {user.statusText}
                      {' '}
                      <Icon name="edit outline" onClick={showEditStatusText} />
                    </div>
                  )}
                </div>
              </HeaderUI>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
      {userStatusText
      && <EditUserStatusText statusText={user.statusText} close={() => setUserStatusText(undefined)} />}
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
