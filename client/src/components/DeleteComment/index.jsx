import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, Grid, Segment, Message } from 'semantic-ui-react';

import styles from './styles.module.scss';

const DeleteComment = ({ commentId, deleteComment, close }) => {
  const [error, setError] = useState(false);

  const handleDeleteComment = async id => {
    try {
      await deleteComment(id);
      close();
    } catch (e) {
      setError(e.message);
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={close}>
      <Modal.Header>
        {!error && (
          <Message className={styles.info} info>
            <Message.Header>Are you serious?</Message.Header>
          </Message>
        )}
        {error && (
          <Message className={styles.info} negative>
            <Message.Header>{error}</Message.Header>
          </Message>
        )}
      </Modal.Header>
      <Modal.Content>
        <Grid columns="equal">
          <Grid.Row>
            <Grid.Column>
              <Segment>
                <Button className={styles.button} negative onClick={close}>Cancel</Button>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
                <Button
                  className={styles.button}
                  positive
                  onClick={() => handleDeleteComment(commentId)}
                >
                  Delete
                </Button>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

DeleteComment.propTypes = {
  deleteComment: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  commentId: PropTypes.string.isRequired
};

export default DeleteComment;

