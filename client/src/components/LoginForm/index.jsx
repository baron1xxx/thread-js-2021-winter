import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment } from 'semantic-ui-react';
import ErrorMessage from '../ErrorMessage';

const LoginForm = ({ login }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [error, setError] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleLoginClick = async () => {
    const isValid = isEmailValid && isPasswordValid;
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await login({ email, password });
    } catch (e) {
      setError(e.message);
      setIsLoading(false);
    }
  };

  return (
    <div>
      {error && <ErrorMessage error={error} close={setError} />}
      <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            error={!isPasswordValid}
            onChange={ev => passwordChanged(ev.target.value)}
            onBlur={() => setIsPasswordValid(Boolean(password))}
          />
          <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
            Login
          </Button>
        </Segment>
      </Form>
    </div>

  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired
};

export default LoginForm;
