import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form } from 'semantic-ui-react';
import { CommentSchema } from '../../helpers/validatorHelper';
import ErrorMessage from '../ErrorMessage';

const AddComment = ({
  postId,
  addComment
}) => {
  const [body, setBody] = useState('');
  const [isBodyValid, setIsBodyValid] = useState(true);
  const [error, setError] = useState(false);

  const handleAddComment = async () => {
    if (!body) {
      return;
    }
    await addComment({ postId, body });
    setBody('');
  };
  const handleChangeComment = async bodyComment => {
    setBody(bodyComment);
    CommentSchema.validate({ body: bodyComment })
      .then(() => {
        setIsBodyValid(true);
        setError(false);
      })
      .catch(({ message }) => {
        setError({ message });
        setIsBodyValid(false);
      });
  };
  return (
    <Form reply onSubmit={handleAddComment}>
      {error && <ErrorMessage error={error.message} close={setError} />}
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => handleChangeComment(ev.target.value)}
      />
      <Button
        type="submit"
        content="Post comment"
        labelPosition="left"
        icon="edit"
        primary
        disabled={!body || !isBodyValid}
      />
    </Form>
  );
};

AddComment.propTypes = {
  addComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export default AddComment;
