import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';
import { ForgotPasswordSchema } from '../../helpers/validatorHelper';
import ErrorMessage from '../ErrorMessage';
import SuccessMessage from '../SuccessMessage';

const ForgotPasswordForm = ({ forgotPassword }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [successMessage, setSuccessMessage] = useState(undefined);
  const [error, setError] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    ForgotPasswordSchema.validate({ email: data })
      .then(() => {
        setIsEmailValid(true);
        setError(false);
      })
      .catch(({ message }) => {
        setIsEmailValid(false);
        setError({ message });
      });
  };
  const handleForgotPasswordClick = async () => {
    setIsLoading(true);
    try {
      const { message } = await forgotPassword(email);
      setSuccessMessage({ message });
      setEmail('');
    } catch ({ message }) {
      setError({ message });
      setIsLoading(false);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div>
      {error && <ErrorMessage error={error.message} close={setError} />}
      {successMessage && <SuccessMessage successMessage={successMessage.message} close={setSuccessMessage} />}
      <Form name="loginForm" size="large" onSubmit={handleForgotPasswordClick}>
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            value={email}
            onChange={ev => emailChanged(ev.target.value)}
          />
          <Button
            type="submit"
            color="teal"
            fluid
            size="large"
            loading={isLoading}
            disabled={!email || !isEmailValid}
            primary
          >
            Forgot password
          </Button>
        </Segment>
      </Form>
    </div>

  );
};

ForgotPasswordForm.propTypes = {
  forgotPassword: PropTypes.func.isRequired
};

export default ForgotPasswordForm;
