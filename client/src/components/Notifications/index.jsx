import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({
  user,
  applyPost,
  applyUpdatePost,
  applyRemovePost,
  applyReactPost,
  applyAddComment,
  applyUpdateComment,
  applyRemoveComment,
  applyReactComment
}) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  const [socket] = useState(io(address));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);
    socket.on('like', () => {
      NotificationManager.info('Your post was liked!');
    });
    socket.on('dislike', () => {
      NotificationManager.info('Your post was disliked!');
    });
    socket.on('new_post', post => {
      if (post.userId !== id) {
        applyPost(post.id);
      }
    });
    socket.on('update_post', postId => {
      applyUpdatePost(postId);
    });
    socket.on('remove_post', postId => {
      applyRemovePost(postId);
    });
    socket.on('post_reacted', postId => {
      applyReactPost(postId);
    });
    socket.on('new_comment', commentId => {
      applyAddComment(commentId);
    });
    socket.on('update_comment', commentId => {
      applyUpdateComment(commentId);
    });
    socket.on('remove_comment', commentId => {
      applyRemoveComment(commentId);
    });
    socket.on('comment_react', commentId => {
      applyReactComment(commentId);
    });

    return () => {
      socket.close();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired,
  applyUpdatePost: PropTypes.func.isRequired,
  applyRemovePost: PropTypes.func.isRequired,
  applyReactPost: PropTypes.func.isRequired,
  applyAddComment: PropTypes.func.isRequired,
  applyUpdateComment: PropTypes.func.isRequired,
  applyRemoveComment: PropTypes.func.isRequired,
  applyReactComment: PropTypes.func.isRequired
};

export default Notifications;
