import React from 'react';
import { List, Image } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';
// TODO Change styles popup user list

const UsersListPopup = ({ reactions }) => (
  <div>
    <div className={styles.reaction}>
      { reactions.map(reaction => (
        <List selection verticalAlign="middle" key={reaction.user.id}>
          <List.Item>
            <Image src={getUserImgLink(reaction.user.image)} size="mini" circular />
            <List.Content>
              <List.Header>
                <div>{reaction.user.username}</div>
                <div>{reaction.user.statusText}</div>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      ))}
    </div>
  </div>
);

UsersListPopup.propTypes = {
  reactions: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default UsersListPopup;
