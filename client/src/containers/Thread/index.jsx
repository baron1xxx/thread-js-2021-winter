import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import EditPost from '../../components/EditPost';
import DeletePost from '../../components/DeletePost';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePost,
  removePost,
  updateComment,
  deleteComment
} from './actions';

import styles from './styles.module.scss';
import EditComment from '../../components/EditComment';
import DeleteComment from '../../components/DeleteComment';

const postsFilter = {
  userId: undefined,
  notEqualUserId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  updatePost: update,
  removePost: remove,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  updateComment: editComment,
  deleteComment: removeComment
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);

  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showPostsILiked, setShowPostsILiked] = useState(false);

  const [editedPost, setEditedPost] = useState(undefined);

  const [deletePost, setDeletePost] = useState(false);
  const [comment, setComment] = useState(false);
  const [commentId, setCommentId] = useState(undefined);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.notEqualUserId = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };
  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    postsFilter.notEqualUserId = hideOwnPosts ? undefined : userId;
    postsFilter.userId = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };
  const toggleShowPostsILiked = () => {
    setShowPostsILiked(!showPostsILiked);
    postsFilter.likedPostsAuthUser = showPostsILiked ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const deletePostId = postId => {
    setDeletePost(postId);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        {!hideOwnPosts && (
          <Checkbox
            toggle
            label="Show only my posts"
            checked={showOwnPosts}
            onChange={toggleShowOwnPosts}
          />
        )}
        {!showOwnPosts && (
          <Checkbox
            toggle
            label="Hide my posts"
            checked={hideOwnPosts}
            onChange={toggleHideOwnPosts}
          />
        )}
        <Checkbox
          toggle
          label="Only posts that I liked"
          checked={showPostsILiked}
          onChange={toggleShowPostsILiked}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            userId={userId}
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            editedPost={setEditedPost}
            deletePost={deletePostId}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          editedPost={setEditedPost}
          removePost={deletePostId}
          showEditComment={setComment}
          commentId={setCommentId}
        />
      )}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {editedPost && (
        <EditPost
          post={editedPost}
          updatePost={update}
          uploadImage={uploadImage}
          close={() => setEditedPost(undefined)}
        />
      )}
      {deletePost
      && (
        <DeletePost
          postId={deletePost}
          deletePost={deletePostId}
          removePost={remove}
          close={() => setDeletePost(undefined)}
        />
      )}
      {comment && (
        <EditComment
          comment={comment}
          updateComment={editComment}
          close={() => setComment(undefined)}
        />
      )}
      {commentId && (
        <DeleteComment
          commentId={commentId}
          deleteComment={removeComment}
          close={() => setCommentId(undefined)}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePost,
  removePost,
  updateComment,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
