import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

// TODO clean code

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};
export const applyUpdatePost = postId => async (dispatch, getRootState) => {
  const editedPost = await postService.getPost(postId);
  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.map(post => (post.id !== editedPost.id
    ? post
    : editedPost));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === editedPost.id) {
    dispatch(setExpandedPostAction(editedPost));
  }
};
export const applyRemovePost = postId => async (dispatch, getRootState) => {
  const deletedPost = await postService.getPost(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.filter(post => post.id !== deletedPost.id);
  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === deletedPost.id) {
    dispatch(setExpandedPostAction(undefined));
  }
};

export const applyReactPost = postId => async (dispatch, getRootState) => {
  const reactedPost = await postService.getPost(postId);
  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : reactedPost));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(reactedPost));
  }
};

export const applyAddComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  const newPost = await postService.getPost(comment.postId);
  const { posts: { posts, expandedPost } } = getRootState();

  const updatePosts = posts.map(post => (post.id !== comment.postId
    ? post
    : newPost));

  dispatch(setPostsAction(updatePosts));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(newPost));
  }
};

export const applyUpdateComment = commentId => async (dispatch, getRootState) => {
  const updatedComment = await commentService.getComment(commentId);
  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;

  const updateComments = comments.map(comment => (comment.id !== updatedComment.id ? comment : updatedComment));

  dispatch(setExpandedPostAction({ ...expandedPost, comments: updateComments }));
};

export const applyRemoveComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  const newPost = await postService.getPost(comment.postId);

  const { posts: { posts, expandedPost } } = getRootState();

  const updatePosts = posts.map(post => (post.id !== newPost.id ? post : newPost));

  dispatch(setPostsAction(updatePosts));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(newPost));
  }
};

export const applyReactComment = commentId => async (dispatch, getRootState) => {
  const reactedComment = await commentService.getComment(commentId);
  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;

  const updateComments = comments.map(comment => (comment.id !== reactedComment.id ? comment : reactedComment));

  dispatch(setExpandedPostAction({ ...expandedPost, comments: updateComments }));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = (id, postBody) => async (dispatch, getRootState) => {
  const { id: postId } = await postService.updatePost(id, postBody);
  const editedPost = await postService.getPost(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== editedPost.id
    ? post
    : editedPost));
  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === editedPost.id) {
    dispatch(setExpandedPostAction(editedPost));
  }
};

export const removePost = id => async (dispatch, getRootState) => {
  const { id: postId } = await postService.removePost(id);
  const deletedPost = await postService.getPost(postId);

  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.filter(post => post.id !== deletedPost.id);

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === deletedPost.id) {
    dispatch(setExpandedPostAction(undefined));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { post: postReact } = await postService.getPostRect(postId);

  const { id } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const mapLikes = post => (
    id && postReact
      ? ({
        ...post,
        likeCount: Number(post.likeCount) + diff,
        dislikeCount: Number(post.dislikeCount) - diff
      })
      : ({
        ...post,
        likeCount: Number(post.likeCount) + diff
      })
  );

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { post: postReact } = await postService.getPostRect(postId);
  const { id } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = post => (
    id && postReact
      ? ({
        ...post,
        likeCount: Number(post.likeCount) - diff,
        dislikeCount: Number(post.dislikeCount) + diff
      })
      : ({
        ...post,
        dislikeCount: Number(post.dislikeCount) + diff
      })
  );

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = (commentId, comment) => async (dispatch, getRootState) => {
  const { id } = await commentService.updateComment(commentId, comment);
  const newComment = await commentService.getComment(id);
  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;
  const updateComments = comments.map(c => (c.id !== newComment.id
    ? c
    : { ...c, body: newComment.body, updatedAt: newComment.updatedAt }));

  dispatch(setExpandedPostAction({ ...expandedPost, comments: updateComments }));
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const { id } = await commentService.deleteComment(commentId);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...(post.comments || [])]
  });

  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    const { comments } = expandedPost;
    const index = comments.findIndex(c => c.id === comment.id);
    comments.splice(index, 1);
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { comment: commentReact } = await commentService.getCommentRect(commentId);
  const { id } = await commentService.likeComment(commentId);

  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = comment => (
    id && commentReact
      ? ({
        ...comment,
        likeCount: Number(comment.likeCount) + diff,
        dislikeCount: Number(comment.dislikeCount) - diff
      })
      : ({
        ...comment,
        likeCount: Number(comment.likeCount) + diff
      })
  );

  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;
  const updateComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  dispatch(setExpandedPostAction({ ...expandedPost, comments: updateComments }));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { comment: commentReact } = await commentService.getCommentRect(commentId);
  const { id } = await commentService.dislikeComment(commentId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = comment => (
    id && commentReact
      ? ({
        ...comment,
        likeCount: Number(comment.likeCount) - diff,
        dislikeCount: Number(comment.dislikeCount) + diff
      })
      : ({
        ...comment,
        dislikeCount: Number(comment.dislikeCount) + diff
      })
  );

  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;
  const updateComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  dispatch(setExpandedPostAction({ ...expandedPost, comments: updateComments }));
};

