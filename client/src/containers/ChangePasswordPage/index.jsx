import React from 'react';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ChangePasswordForm from 'src/components/ChangePasswordForm';
import * as authService from 'src/services/authService';
import PropTypes from 'prop-types';

const ChangePasswordPage = ({ match }) => {
  const changePassword = (password, token) => authService.changePassword(password, token);
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Change password
        </Header>
        <ChangePasswordForm token={match.params.token} changePassword={changePassword} />
        <Message>
          Alredy with us?
          {' '}
          <NavLink exact to="/login">Sign In</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

ChangePasswordPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any)
};
ChangePasswordPage.defaultProps = {
  match: undefined
};

export default ChangePasswordPage;
