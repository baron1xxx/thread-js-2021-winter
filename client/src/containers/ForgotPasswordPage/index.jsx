import React from 'react';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ForgotPasswordForm from 'src/components/ForgotPasswordForm';
import * as authService from 'src/services/authService';

const ForgotPasswordPage = () => {
  const forgotPassword = email => authService.forgotPassword({ email });
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Forgot password
        </Header>
        <ForgotPasswordForm forgotPassword={forgotPassword} />
        <Message>
          Alredy with us?
          {' '}
          <NavLink exact to="/login">Sign In</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

export default ForgotPasswordPage;
