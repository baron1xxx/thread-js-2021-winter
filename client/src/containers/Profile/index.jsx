import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/lib/ReactCrop.scss';
import { Button, Grid, Icon, Image, Input, Label } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import * as imageService from 'src/services/imageService';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { updateProfile } from './actions';
import { UpdateUserSchema } from '../../helpers/validatorHelper';
import { getCroppedImg } from '../../helpers/croperImageHelper';
import ErrorMessage from '../../components/ErrorMessage';

import styles from './styles.module.scss';

const Profile = ({ user, updateProfile: updateUser }) => {
  const { id, username, email, statusText } = user;

  const [userName, setUserName] = useState(username);
  const [userEmail, setUserEmail] = useState(email);
  const [userStatusText, setUserStatusText] = useState(statusText);

  const [disabledUserName, setDisabledUserName] = useState(true);
  const [disabledEmail, setDisabledEmail] = useState(true);
  const [disabledStatusText, setDisabledStatusText] = useState(true);

  const [image, setImage] = useState({ imageId: user.image?.id, imageLink: user.image?.link });
  const [isUploading, setIsUploading] = useState(false);

  const [crop, setCrop] = useState({ aspect: 1 / 1, height: 200, width: 200, keepSelection: true });
  const [loadImage, setLoadImage] = useState(undefined);
  const [srcLoadImage, setSrcLoadImage] = useState(undefined);
  const [croppedImage, setCroppedImage] = useState(undefined);

  const [error, setError] = useState(false);

  const validateUser = data => {
    UpdateUserSchema.validate(data)
      .then(() => {
        setError(false);
      })
      .catch(({ message }) => {
        setError({ message });
      });
  };

  const changeUserName = value => {
    setUserName(value);
    validateUser({ username: value });
  };
  const changeEmail = value => {
    setUserEmail(value);
    validateUser({ email: value });
  };
  const changeStatusText = value => {
    setUserStatusText(value);
    validateUser({ statusText: userStatusText });
  };

  const editUserName = async isDisabled => {
    setDisabledUserName(isDisabled);
    try {
      if (isDisabled) {
        await updateUser(id, { username: userName });
      }
    } catch (e) {
      setError({ message: e.message });
    }
  };

  const editEmail = async isDisabled => {
    setDisabledEmail(isDisabled);
    try {
      if (isDisabled) {
        await updateUser(id, { email: userEmail });
      }
    } catch (e) {
      setError({ message: e.message });
    }
  };

  const editStatusText = async isDisabled => {
    setDisabledStatusText(isDisabled);
    try {
      if (isDisabled) {
        await updateUser(id, { statusText: userStatusText });
      }
    } catch (e) {
      setError({ message: e.message });
    }
  };

  const uploadImage = file => imageService.uploadImage(file);

  const handleUploadFile = async ({ target }) => {
    setSrcLoadImage(URL.createObjectURL(target.files[0]));
  };

  const handleGetCroppedImg = async () => {
    const croppedImg = await getCroppedImg(loadImage, crop);
    const croppedImageURL = URL.createObjectURL(croppedImg);
    setImage({ imageLink: croppedImageURL });
    setCroppedImage(croppedImg);
  };
  const handleCancelCroppedImg = () => {
    setImage({ imageId: user.image.id, imageLink: user.image.link });
    setSrcLoadImage(undefined);
    setLoadImage(undefined);
  };

  const handleUpdateImage = async () => {
    setIsUploading(true);
    try {
      const { id: imageId } = await uploadImage(croppedImage);
      await updateUser(id, { imageId });
      setSrcLoadImage(undefined);
      setLoadImage(undefined);
    } catch (e) {
      setError({ message: e.message });
      setIsUploading(false);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={image?.imageLink || getUserImgLink(user.image)} size="medium" circular />
        <Button color="teal" icon labelPosition="left" as="label">
          <Icon name="image" />
          Upload
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <br />
        <br />
        {srcLoadImage
          && (
            <div>
              <Button color="red" icon labelPosition="left" as="label" onClick={() => handleCancelCroppedImg()}>
                <Icon name="cancel" />
                Cancel
              </Button>
              <Button color="orange" icon labelPosition="left" as="label" onClick={() => handleGetCroppedImg()}>
                <Icon name="user circle" />
                Preview
              </Button>
              <Button
                color="blue"
                icon
                labelPosition="left"
                as="label"
                loading={isUploading}
                disabled={isUploading || !croppedImage}
                onClick={() => handleUpdateImage()}
              >
                <Icon name="" />
                Update
              </Button>
            </div>
          )}
        <br />
        <div>
          {srcLoadImage && (
            <ReactCrop
              src={srcLoadImage}
              onImageLoaded={setLoadImage}
              crop={crop}
              onChange={setCrop}
            />
          )}
        </div>
        <br />
        <br />
        {error && <ErrorMessage error={error.message} close={setError} />}
        <div>
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            disabled={disabledUserName}
            value={userName}
            onChange={ev => changeUserName(ev.target.value)}
            onBlur={() => editUserName(true)}
          />
          <Label basic size="large" as="a" className={styles.toolbarBtn} onClick={() => editUserName(false)}>
            <Icon name="edit" />
          </Label>
        </div>
        <br />
        <br />
        <div>
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            disabled={disabledEmail}
            value={userEmail}
            onChange={ev => changeEmail(ev.target.value)}
            onBlur={() => editEmail(true)}
          />
          <Label basic size="large" as="a" className={styles.toolbarBtn} onClick={() => editEmail(false)}>
            <Icon name="edit" />
          </Label>
        </div>
        <br />
        <br />
        <div>
          <Input
            icon="text cursor"
            iconPosition="left"
            placeholder="Status text"
            type="text"
            disabled={disabledStatusText}
            value={userStatusText}
            onChange={ev => changeStatusText(ev.target.value)}
            onBlur={() => editStatusText(true)}
          />
          <Label basic size="large" as="a" className={styles.toolbarBtn} onClick={() => editStatusText(false)}>
            <Icon name="edit" />
          </Label>
        </div>

      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateProfile: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateProfile
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
