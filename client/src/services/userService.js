import callWebApi from 'src/helpers/webApiHelper';

export const getById = async userId => {
  const response = await callWebApi({
    endpoint: `/api/users/${userId}`,
    type: 'GET'
  });
  return response.json();
};

export const update = async (id, request) => {
  const response = await callWebApi({
    endpoint: `/api/users/${id}`,
    type: 'PUT',
    request
  });
  return response.json();
};

