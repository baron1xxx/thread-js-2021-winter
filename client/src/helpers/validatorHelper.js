import * as yup from 'yup';

export const CommentSchema = yup.object().shape({
  body: yup.string()
    .required('body is required')
    .min(1, 'Min 1 symbol')
    .max(200, 'Max 200 symbol')
    .matches(/^[a-zA-Z0-9?$@#()\\'!,+/\-=_:.&€£*%\s]+$/, 'Does not match pattern.')
});

export const PostSchema = yup.object().shape({
  body: yup.string()
    .required('body is required')
    .min(1, 'Min 1 symbol')
    .max(200, 'Max 200 symbol')
    .matches(/^[a-zA-Z0-9?$@#()\\'!,+/\-=_:.&€£*%\s]+$/, 'Does not match pattern.')
});

export const UpdateUserSchema = yup.object().shape({
  username: yup.string()
    .min(3, 'Username min 3 symbol')
    .max(20, 'Username max 20 symbol')
    .matches(/^[a-zA-Z0-9?$@#()\\'!,+/\-=_:.&€£*%\s]+$/, 'Does not match pattern.'),
  email: yup.string()
    .email('Email does not match pattern!!!'),
  statusText: yup.string()
    .min(3, 'Status text min 3 symbol')
    .max(100, 'Status text max 100 symbol')
    .matches(/^[a-zA-Z0-9?$@#()\\'!,+/\-=_:.&€£*%\s]+$/, 'Does not match pattern.')
});

export const ForgotPasswordSchema = yup.object().shape({
  email: yup.string()
    .required('Email is required')
    .email('Email does not match pattern.')
});

export const ChangePasswordSchema = yup.object().shape({
  password: yup.string()
    .required('Password is required')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,})/,
      'Password not match pattern.'
    ),
  passwordConfirm: yup.string()
    .oneOf([yup.ref('password'), null], 'Passwords must match')
});
